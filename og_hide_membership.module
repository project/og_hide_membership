<?php

/**
 * @file
 * Hide one's group membership from group roster.
 */

/**
 * Implementation of hook_perm().
 */
function og_hide_membership_perm() {
  return array('access og hide membership');
}

function og_hide_membership_form_og_manage_form_alter(&$form, &$form_state){
  global $user;

  if (!user_access('access og hide membership')) {
    return;
  }

  $form['hide_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide my membership to this group from others.'),
    '#default_value' => $user->og_groups[$form['gid']['#value']]['is_active'] > 1 ? 1 : 0,
    '#return_value' => 1,
    '#weight' => 0,
  );
  // If submit button is missing, add one.
  if (!$form['op']) {
    $form['op'] = array('#type' => 'submit', '#value' => t('Submit'));
  }
  $form['#submit'][] = 'og_hide_membership_manage_form_submit';
}

function og_hide_membership_manage_form_submit($form, &$form_state) {
  global $user;
  $passed_values = $form_state['values'];
  unset($passed_values['gid'], $passed_values['op'], $passed_values['form_id'], $passed_values['form_build_id'], $passed_values['form_token']);
  og_save_subscription($form_state['values']['gid'], $user->uid, $passed_values);

  if (isset($form_state['values']['hide_active'])) {
    db_query("UPDATE {og_uid} SET is_active = %d WHERE nid = %d AND uid = %d",
      $form_state['values']['hide_active'] ? 2 : 1,
      $form_state['values']['gid'], $user->uid
    );

    db_query("DELETE FROM {cache_page} WHERE cid = '%s'", "http://{$_SERVER[HTTP_HOST]}/user/{$user->uid}");
  }
  
  drupal_set_message(t('OG Hide Membership setting saved.'));
}

/**
 * Implementation of hook_og_links_alter().
 * Overrides og_views_og_links_alter() to show member count excluding hidden ones.
 */
function og_hide_membership_og_links_alter(&$links, $node) {
  if (isset($links['subscribers'])) {
    $count = db_result(db_query("SELECT COUNT(*) FROM {og_uid} ou INNER JOIN {users} u ON ou.uid = u.uid WHERE ou.nid = %d AND u.status > 0 AND ou.is_active = 1 AND ou.is_admin >= 0", $node->nid));
    
    $txt = format_plural($count, '1 member', '@count members');
    $links['subscribers'] = og_is_picture() ? l($txt, "og/users/$node->nid/faces") : l($txt, "og/users/$node->nid");
  }
}

/**
 * Implementation of hook_views_pre_execute(); provided by Views module.
 * Override Views 'og_members' and 'og_members_faces' to alter sql statement ro hide hidden members (is_active = 2).
 */
function og_hide_membership_views_pre_execute(&$view) {
  if ($view->name == 'og_members') {
    $view->build_info['query'] = str_replace('(users.status <> 0)', '(users.status <> 0) AND (og_uid.is_active <= 1)', $view->build_info['query']);
    $view->build_info['count_query'] = str_replace('(users.status <> 0)', '(users.status <> 0) AND (og_uid.is_active <= 1)', $view->build_info['count_query']);
  }
  elseif ($view->name == 'og_members_faces') {
    $view->build_info['query'] = str_replace('og_uid.is_active <> 0', 'og_uid.is_active = 1', $view->build_info['query']);
    $view->build_info['count_query'] = str_replace('og_uid.is_active <> 0', 'og_uid.is_active = 1', $view->build_info['count_query']);
  }
}